from logger import Logger
import network
import config
from time import sleep_ms
import protocol
import ubinascii


class WIFIError(Exception):
    pass


def connect_wifi():
    retry_counter = 0
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    Logger.log('connecting to network...')
    wlan.connect(config.WIFI_SSID, config.WIFI_PASSWORD)
    while not wlan.isconnected():
        if retry_counter == config.MAX_RETRY_COUNT:
            raise WIFIError
        retry_counter += 1
        sleep_ms(1000)
    Logger.log('network config:',)
    Logger.log(wlan.ifconfig())


def get_mac_address():
    return ubinascii.hexlify(network.WLAN().config('mac'), ':').decode()


def create_message(operation, payload=None):
    return protocol.Message(
        get_mac_address(),
        config.DEVICE_TYPE,
        config.SERVER_ID,
        operation,
        payload
    )
