import config


class Logger:
    @staticmethod
    def log(msg):
        if config.ENABLE_LOGGER:
            print(msg)
