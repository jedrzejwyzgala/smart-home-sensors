from .device import Device
from logger import Logger
from _thread import start_new_thread
import utils
import protocol.operations
from machine import Pin, ADC
from time import sleep_ms


class LightSensor(Device):
    MAX_LIGHT_VALUE = 4095
    SEND_DATA_PERIOD_MS = 10000

    def __init__(self):
        super(LightSensor, self).__init__()
        self.light_sensor = ADC(Pin(33))
        self.light_sensor.atten(ADC.ATTN_11DB)

    def run(self):
        super(LightSensor, self).run()
        start_new_thread(self.__send_periodically, (self.SEND_DATA_PERIOD_MS,))

    def __get_light_value(self):
        raw_light_value = self.light_sensor.read()
        light_percentage = 100 - int(float(raw_light_value) / self.MAX_LIGHT_VALUE * 100)
        Logger.log(light_percentage)
        return light_percentage

    def __send(self):
        light_value = self.__get_light_value()
        message = utils.create_message(protocol.operations.SENSOR_DATA, {'value': light_value})
        self.server.send(message)

    def _on_command(self, message):
        try:
            if message.payload['command'] == 'getValue':
                self.__send()
            else:
                Logger.log('Unknown command')
        except KeyError:
            Logger.log('No command specified')

    def __send_periodically(self, period):
        while True:
            self.__send()
            sleep_ms(period)
