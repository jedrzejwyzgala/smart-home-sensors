from server import Server
import protocol.operations
from _thread import start_new_thread
import protocol.operations
from logger import Logger
from finder import find_server_ip


class Device:
    def __init__(self):
        server_addr = find_server_ip()
        self.server = Server(server_addr)
        self.server.connect()
        self.server.register()

    def run(self):
        start_new_thread(self.__listen, ())

    def __listen(self):
        while True:
            message = self.server.receive()
            if message.operation == protocol.operations.COMMAND:
                self._on_command(message)
            else:
                Logger.log('Unexpected operation: {0}'.format(message.operation))

    def _on_command(self, message):
        Logger.log('Got command: {0}'.format(message.payload['command']))
