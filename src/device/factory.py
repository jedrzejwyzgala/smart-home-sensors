import config
from .light_sensor import LightSensor
from .switch import Switch
import device_types


class UnknownDeviceError(Exception):
    pass


class DeviceFactory:
    @staticmethod
    def create():
        if config.DEVICE_TYPE == device_types.SWITCH:
            return Switch()
        elif config.DEVICE_TYPE == device_types.LIGHT_SENSOR:
            return LightSensor()

        raise UnknownDeviceError()
