from .device import Device
from logger import Logger
from _thread import start_new_thread
import utils
import protocol.operations
from machine import Pin


class Switch(Device):
    def __init__(self):
        super(Switch, self).__init__()
        self.switch = Pin(33, Pin.OUT)
        self.switch.on()

    def _on_command(self, message):
        try:
            if message.payload['command'] == 'setValue':
                self.switch.value(message.payload['value'])
                start_new_thread(self.__send(), ())
            elif message.payload['command'] == 'getValue':
                start_new_thread(self.__send(), ())
            else:
                Logger.log('Unknown command')
        except KeyError:
            Logger.log('No command specified')

    def __send(self):
        light_value = self.switch.value()
        Logger.log('Sending value {0}'.format(light_value))
        message = utils.create_message(protocol.operations.SENSOR_DATA, {'value': light_value})
        self.server.send(message)
