import esp_utils
import utils
from logger import Logger
from device import DeviceFactory
from time import sleep_ms


try:
    esp_utils.blink_fast(esp_utils.blue)
    utils.connect_wifi()
    esp_utils.all_led_off()
    esp_utils.stop_blinking(esp_utils.blue)
    esp_utils.green.on()
except utils.WIFIError:
    Logger.log('Cannot connect to WIFI')
    esp_utils.stop_blinking(esp_utils.blue)
    esp_utils.error_and_exit()

DeviceFactory.create().run()
