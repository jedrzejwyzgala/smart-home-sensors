from usocket import socket, AF_INET, SOCK_STREAM
import protocol
import protocol.operations
import config
from logger import Logger
import utils


class ServerError(Exception):
    pass


class Server:
    def __init__(self, ip):
        self.ip = ip
        self._connection = None

    def connect(self):
        self._connection = socket(AF_INET, SOCK_STREAM)
        self._connection.connect((self.ip, config.SERVER_TCP_IN_PORT))

    def send(self, message):
        try:
            self._connection.send(protocol.encode(message))
        except OSError as e:
            Logger.log(e)

    def receive(self):
        try:
            data = self._connection.recv(1024)
            try:
                return protocol.decode(data)
            except protocol.ParserError:
                Logger.log('Parser error')

        except OSError as e:
            Logger.log(e)

    def register(self):
        message = utils.create_message(protocol.operations.REGISTER)
        self.send(message)
        response = self.receive()  # TODO ?
