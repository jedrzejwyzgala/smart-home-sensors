import config
from logger import Logger
from usocket import socket, AF_INET, SOCK_DGRAM, SOL_SOCKET, SO_REUSEADDR, IPPROTO_UDP
import protocol.operations
import utils
import device_types


def find_server_ip():
    Logger.log('Searching for a server...')
    udp_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)  # create UDP socket
    udp_socket.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)  # this is a broadcast socket
    # udp_socket.setsockopt(SOL_SOCKET, SO_BROADCAST, 1)
    in_socket = socket(AF_INET, SOCK_DGRAM)  # create UDP socket
    in_socket.bind(('', config.UDP_PORT))

    msg = utils.create_message(protocol.operations.HELLO, {'port': config.UDP_PORT})
    data = protocol.encode(msg)
    Logger.log('Sending: {0}'.format(data))
    udp_socket.sendto(data, (config.SERVER_IP, config.SERVER_UDP_IN_PORT))  # TODO broadcast

    Logger.log('Waiting for response')
    data, addr = in_socket.recvfrom(1024)  # wait for a packet
    response = protocol.decode(data)
    if response.sender_type == device_types.SERVER and response.sender_id == config.SERVER_ID:
        Logger.log('New server: {0}'.format(addr))
        return addr[0]
    else:
        Logger.log('Message received is not from server: {0}'.format(addr))
