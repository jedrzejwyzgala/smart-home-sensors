from machine import Pin, Signal, Timer
import sys
import config


red = Signal(Pin(config.RED_PIN, Pin.OUT), invert=config.RED_PIN_INVERSE)
red.off()
green = Signal(Pin(config.GREEN_PIN, Pin.OUT), invert=config.GREEN_PIN_INVERSE)
green.off()
blue = Signal(Pin(config.BLUE_PIN, Pin.OUT), invert=config.BLUE_PIN_INVERSE)
blue.off()

_timers = {}


def all_led_off():
    red.off()
    green.off()
    blue.off()


def error_and_exit():
    all_led_off()
    red.on()
    sys.exit()


def toggle(led):
    led.value(not led.value())


def _toggle_callback(led):
    toggle(led)


def _blink(led, freq):
    led.off()
    timer = Timer(-1)
    timer.init(mode=Timer.PERIODIC, freq=freq, callback=lambda t: _toggle_callback(led))
    _timers[id(led)] = timer


def blink_fast(led):
    _blink(led, 4)


def blink_slow(led):
    _blink(led, 2)


def stop_blinking(led):
    try:
        timer = _timers.pop(id(led))
        timer.deinit()
        led.off()
    except KeyError:
        pass
