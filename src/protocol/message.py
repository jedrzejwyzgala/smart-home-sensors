class Message:
    def __init__(self, sender_id, sender_type, receiver_id, operation, payload=None):
        self.sender_id = sender_id
        self.sender_type = sender_type
        self.receiver_id = receiver_id
        self.operation = operation
        self.payload = payload if payload else {}
