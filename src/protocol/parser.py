from .message import Message
import protocol.fields as fields
import json


class ParserError(Exception):
    pass


def encode(message: Message) -> bytes:
    result = message.payload.copy()
    result.update({
        fields.SENDER_ID: message.sender_id,
        fields.SENDER_TYPE: message.sender_type,
        fields.RECEIVER_ID: message.receiver_id,
        fields.OPERATION: message.operation,
    })

    str_result = json.dumps(result)
    return str_result.encode()


def decode(raw_data: bytes) -> Message:
    try:
        str_payload = raw_data.decode()
        payload = json.loads(str_payload)
        return Message(
            payload.pop(fields.SENDER_ID),
            payload.pop(fields.SENDER_TYPE),
            payload.pop(fields.RECEIVER_ID),
            payload.pop(fields.OPERATION),
            payload
        )

    except (json.JSONDecodeError, KeyError):
        raise ParserError
